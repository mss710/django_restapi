from django.apps import AppConfig


class DjangoppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'djangopp'
