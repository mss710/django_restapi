from django.db import models
from django.contrib.postgres.fields import ArrayField
# Create your models here.
class Employeedetail(models.Model):
    fullname = models.CharField(max_length=20)
    emp_code = models.CharField(max_length=5)
    mobile = models.CharField(max_length=10)
    skill = ArrayField(models.CharField(max_length=200, blank=True),size=8)